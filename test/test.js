let copy = (() => {
	var _ref = _asyncToGenerator(function* (from, to) {
		xauto = yield Xauto();
		yield xauto.plugin("file");
		let task = yield xauto.task([[["file:read", { dir: from }], ["file:write", { dir: to }]]], 'file');
		let watcher = yield xauto.watcher(["file:watch", { dir: "from" }], "file");
		yield watcher(task);
	});

	return function copy(_x, _x2) {
		return _ref.apply(this, arguments);
	};
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const Xauto = require("../");
let xauto;


copy('from', 'to').catch(console.log);