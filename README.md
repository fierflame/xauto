![Xauto](doc/image/xauto.png)  
Xauto 是易扩展的自动化工具  
[![npm|Xauto](https://img.shields.io/npm/v/xauto.svg)](https://www.npmjs.com/package/xauto)
[![npm|Xauto](https://img.shields.io/npm/l/xauto.svg)](https://www.npmjs.com/package/xauto)
[![npm|Xauto](https://img.shields.io/npm/dt/xauto.svg)](https://www.npmjs.com/package/xauto)

##安装Xauto
使用npm 安装

```
npm install xauto -g
```

##系统依赖
node 6.9.0 或更高版本。

##帮助文档
* [在CLI中使用 Xauto](doc/cli.md)
* [Xauto 描述](doc/description.md)
* [Xauto API](doc/api.md)
* [Xauto 插件开发](doc/plugin.md)
