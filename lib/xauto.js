
/**
 * 异步记载插件模块
 * @param	{String}	name		插件名称
 * @param	{String}	searchPath	插件搜索开始目录
 * @param	{Boolean}	recursion	是否递归父路径搜索
 * @param	{Array}		prefix		插件名称前缀
 * @return	{Object}				插件
 */
let loadPluginModule = (() => {
	var _ref = _asyncToGenerator(function* (name, searchPath = "", ...opt) {
		if (typeof opt[0] !== "boolean") {
			opt.splice(0, 0, true);
		}
		if (!(opt[1] instanceof Array)) {
			opt.splice(1, 0, []);
		}
		let [recursion, prefix] = opt;
		prefix = prefix.filter(function (s) {
			return s && typeof s === "string";
		});
		if (!prefix.length) {
			prefix = ["/node_modules/xauto-plugin-", "/xauto-plugin/", "/xauto-plugin-", "/"];
		}
		try {
			return require("xauto-plugin-" + name);
		} catch (e) {}
		let l = prefix.length;
		for (;;) {
			for (let i = 0; i < l; i++) {
				try {
					return require(searchPath + prefix[i] + name);
				} catch (e) {}
			}
			if (!recursion) {
				break;
			}
			//到父页面搜索
			let np = path.dirname(searchPath);
			if (np === searchPath) {
				break;
			}
			searchPath = np;
		};
		return null;
	});

	return function loadPluginModule(_x) {
		return _ref.apply(this, arguments);
	};
})();
/**
 * 创建者
 * @param	{Array}		options.task	任务列表
 * @param	{Boolean}	options.async	是否为异步任务
 * @param	{Boolean}	options.strict	是否严格要求任务有效性
 * @return	{Function}					任务函数
 */


let baseCreator = (() => {
	var _ref2 = _asyncToGenerator(function* ({
		task,
		async: parallel,
		strict
	}) {
		var _this = this;

		task = yield Promise.all(task.map(function (t) {
			return _this.task(t);
		}));
		if (strict) {
			task.map(function (x) {
				if (!x) {
					task = null;
				}
			});
		} else {
			task = task.filter(function (x) {
				return x;
			});
		}
		if (!task) {
			return {};
		}
		if (parallel) {
			return function (arg) {
				return Promise.all(task.map(function (x) {
					return x(arg, _this);
				}));
			};
		}
		return (() => {
			var _ref3 = _asyncToGenerator(function* (arg) {
				for (let i = 0, l = task.length; i < l; i++) {
					arg = yield task[i](arg, this);
				}
				return arg;
			});

			return function (_x3) {
				return _ref3.apply(this, arguments);
			};
		})();
	});

	return function baseCreator(_x2) {
		return _ref2.apply(this, arguments);
	};
})();

//默认创建者


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const path = require("path");
const isName = (_ => {
	const NAME = /^[a-zA-Z0-9\u4e00-\u9fa5\:\-_\/]+$/;
	return function isName(name) {
		if (typeof name !== "string") {
			return false;
		}
		return NAME.test(name);
	};
})();const Xcreator = {
	base: baseCreator
};

//创建者代理配置
const creatorProxy = {
	apply(creator, object, args) {
		return creator.create(...args);
	},
	construct(creator, args) {
		return creator.new(...args);
	}
};
//任务代理配置
const taskProxy = {
	apply(task, object, args) {
		if (object instanceof Context) {
			if (args[1] instanceof Context) {
				args[1] = object;
			} else {
				args.splice(1, 0, object);
			}
		}
		return task.exec(...args);
	},
	construct(task, args) {
		return task.new(...args);
	}
};
//监视者代理配置
const watcherProxy = {
	apply(watcher, object, args) {
		return watcher.watch(...args);
	},
	construct(watcher, args) {
		return watcher.new(...args);
	}
};
//监视状态代理配置
const watchStateProxy = {
	apply(watchState, object, args) {
		return watchState.stop(...args);
	},
	construct(watchState, args) {
		return watchState.new(...args);
	}
};

/**
 * Xauto代理
 */
let XautoProxy = {
	apply(Xauto, object, [{
		opt, pkg, echo, XBWP, XPBP, path,
		plugin = [], plugins = [],
		creator = {}, creators = {},
		task = {}, tasks = {},
		watcher = {}, watchers = {},
		exec = [],
		watch = {},
		script
	} = {}]) {
		return _asyncToGenerator(function* () {
			const xauto = new Xauto({ opt, pkg, echo, XBWP, XPBP, path });

			if (!(plugin instanceof Array)) {
				plugin = [];
			}
			if (!(plugins instanceof Array)) {
				plugins = [];
			}
			if (typeof creator !== "object" || !creator) {
				creator = {};
			}
			if (typeof creators !== "object" || !creators) {
				creators = {};
			}
			if (typeof task !== "object" || !task) {
				task = {};
			}
			if (typeof tasks !== "object" || !tasks) {
				tasks = {};
			}
			if (typeof watcher !== "object" || !watcher) {
				watcher = {};
			}
			if (typeof watchers !== "object" || !watchers) {
				watchers = {};
			}
			if (!(exec instanceof Array)) {
				exec = [];
			}
			if (typeof watch !== "object" || !watch) {
				watch = {};
			}
			if (typeof script !== "string" || !script) {
				script = '';
			}

			for (let k in Xcreator) {
				yield xauto.creator(Xcreator[k], `Xauto:${k}`);
			}

			for (let i = 0, l = plugin.length; i < l; i++) {
				yield xauto.plugin(plugin[i]);
			}
			for (let i = 0, l = plugins.length; i < l; i++) {
				yield xauto.plugin(plugins[i]);
			}

			for (let k in creator) {
				yield xauto.creator(creator[k], k);
			}
			for (let k in creators) {
				yield xauto.creator(creators[k], k);
			}
			for (let k in task) {
				yield xauto.task(task[k], k);
			}
			for (let k in tasks) {
				yield xauto.task(tasks[k], k);
			}
			for (let k in watcher) {
				yield xauto.watcher(watcher[k], k);
			}
			for (let k in watchers) {
				yield xauto.watcher(watchers[k], k);
			}
			exec.map(function (e) {
				return xauto.exec(e);
			});
			for (let k in watch) {
				let ws = watch[k];
				if (!(ws instanceof Array)) {
					continue;
				}
				xauto.register((yield xauto.watch(...ws)), k);
			}
			if (script) {
				script = path.resolve(xauto.XBWP, script);
				try {
					script = require(script);
				} catch (e) {}
				if (typeof script === "function") {
					try {
						script(Xauto);
					} catch (e) {}
				}
			}
			return xauto;
		})();
	}
};

/**
 * 信息输出(默认)
 * @param  {String} options.lv 等级
 * @param  {String} options.tp 类型
 * @param  {String} options.at 所在模块
 * @param  {String} options.fn 文件名
 * @param  {Number} options.ln 行号
 * @param  {Number} options.cl 列号
 * @param  {String} options.tx 错误信息
 */
function print({ lv, tp, at, fn, ln, cl, tx }) {
	if (tp) {
		tp = ":" + tp;
	}
	if (at) {
		at = "@" + at;
	}
	if (path.isAbsolute(fn)) {
		fn = path.relative(this.XBWP, fn);
	}
	tx.replace(/\\/g, "\\\\").replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\f/g, "\\f");
	console.log(`${fn}:${ln}:${cl}: [${lv}${tp}${at}]${tx}`);
}

const XautoSignal = (() => {
	//信号代理配置
	const SignalProxy = {
		apply(Signal, object, [signal]) {
			if (signal instanceof Signal) {
				throw signal;
			}
		}
	};
	class XautoSignal extends Error {}
	class XautoExitSignal extends XautoSignal {}
	class XautoErrorSignal extends XautoSignal {
		constructor({ lv = "info", tp, at, fn, ln, cl, tx } = {}) {
			super();
			this.lv = lv;
			this.tp = tp;
			this.at = at;
			this.fn = fn;
			this.ln = ln;
			this.cl = cl;
			this.tx = tx;
		}
	}
	const Signal = new Proxy(XautoSignal, SignalProxy);
	Signal.exit = Signal.Exit = new Proxy(XautoExitSignal, SignalProxy);
	Signal.error = Signal.Error = new Proxy(XautoErrorSignal, SignalProxy);
	return Signal;
})();

/**
 * 上下文this
 */
const Context = (_ => {
	let contextProxy = {
		apply(context, object, args) {
			context.echo(...args);
		},
		setPrototypeOf() {
			return false;
		},
		getPrototypeOf() {
			return Context.prototype;
		}
	};
	let proxy = {
		apply(Context, object, [c, x]) {
			if (c instanceof Context) {
				return c;
			}
			return proxy.construct(Context, [x]);
		},
		construct(Context, [xauto]) {
			let context = new Context();
			Object.setPrototypeOf(context, xauto);
			//设置上下文专用量
			Object.defineProperties(context, { $: { value: {}, enumerable: true } });
			return new Proxy(context, contextProxy);
		}
	};
	return new Proxy(class Context extends Function {}, proxy);
})();
/**
 * 任务
 * @function	new(...args)			基于当前任务创建新的任务
 * @function	exec(arg, [context])	在特定的上下文执行任务
 * @attribute	Xauto					所属的Xauto
 * @attribute	_task					任务函数
 * @attribute	_opt					任务选项
 * @attribute	_args					任务参数
 */
class Task extends Function {
	constructor(task, opt, ...args) {
		super();
		if (typeof opt !== "object") {
			opt = {};
		}
		if (task instanceof Task) {
			this._task = task._task;
			this.Xauto = task.Xauto;
			this._opt = Object.assign({}, task._opt, opt);
			let _args = this._args = Array.from(task._args);
			args.map(x => _args.push(x));
		} else if (task instanceof Array && isFunction(task[0]) && task[1] instanceof Xauto) {
			this._task = task[0];
			this.Xauto = task[1];
			this._opt = Object.assign({}, opt);
			let _args = this._args = [];
			args.map(x => _args.push(x));
		} else {
			throw new Error("非法的task");
		}
	}
	/**
  * 执行此任务
  * @param  {any} arg     任务的参数
  * @param  {Context} context 上下文this
  * @return {any}         任务返回值
  */
	exec(arg, context) {
		var _this2 = this;

		return _asyncToGenerator(function* () {
			return yield _this2._task.call(Context(context, _this2.Xauto), arg, _this2._opt, ..._this2._args);
		})();
	}
	/**
  * 基于当前任务创建
  * @param  {any}    opt  选项
  * @param  {...any} args 参数
  * @return {Task}        创建的任务
  */
	new(...args) {
		return new Proxy(new Task(this, ...args), taskProxy);
	}
}
/**
 * 监视状态
 * @function	new(...args)	基于当前监视状态的监视者创建新的监视者
 * @function	emit(arg)		触发任务
 * @function	stop()			停止状态
 * @attribute	Xauto			所属的Xauto
 * @attribute	watcher			监视者
 * @attribute	watching		监视者状态
 * @attribute	_watcher		监视者
 * @attribute	_task			任务
 * @attribute	_watching		监视者状态
 * @attribute	_handle			监视停止函数
 */
class WatchState extends Function {
	constructor() {
		super();
		this._watching = true;
	}
	get watching() {
		return this._watching;
	}
	get watcher() {
		return this._watcher;
	}
	emit(arg) {
		var _this3 = this;

		return _asyncToGenerator(function* () {
			if (!_this3._watching) {
				return;
			}
			try {
				yield _this3._task(arg);
			} catch (e) {
				handle.call(_this3.Xauto, e);
				return false;
			}
			return true;
		})();
	}
	stop() {
		var _this4 = this;

		return _asyncToGenerator(function* () {
			if (!_this4._watching) {
				return;
			}
			_this4._watching = false;
			try {
				_this4._handle.call(null);
			} catch (e) {}
		})();
	}
	new(...args) {
		return this._watcher.new(...args);
	}
}
/**
 * 监视者
 * @function	new(...args)	基于当前监视者创建新的监视者
 * @function	watch(task)		开始监视
 * @attribute	Xauto			所属的Xauto
 * @attribute	_watcher		监视者函数
 * @attribute	_opt			监视者选项
 * @attribute	_args			监视者参数
 */
class Watcher extends Function {
	constructor(watcher, opt, ...args) {
		super();
		if (typeof opt !== "object") {
			opt = {};
		};
		if (watcher instanceof Watcher) {
			this._watcher = watcher._watcher;
			this.Xauto = watcher.Xauto;
			this._opt = Object.assign({}, watcher._opt, opt);
			let _args = this._args = Array.from(watcher._args);
			args.map(x => _args.push(x));
		} else if (watcher instanceof Array && isFunction(watcher[0]) && watcher[1] instanceof Xauto) {
			this._watcher = watcher[0];
			this.Xauto = watcher[1];
			this._opt = Object.assign({}, opt);
			let _args = this._args = [];
			args.map(x => _args.push(x));
		} else {
			throw new Error("非法的watcher");
		}
	}
	/**
  * 开始监听
  * @param  {Task}		task	事件发生后执行的任务
  * @return {WatchState}			产生的事件监听
  */
	watch(task) {
		var _this5 = this;

		return _asyncToGenerator(function* () {
			if (!(task = yield _this5.Xauto.task(task))) {
				return null;
			}
			if (task.Xauto !== _this5.Xauto) {
				throw new Error("监视者与任务属于同一个Xauto");
			}
			let ws = new WatchState();
			ws.Xauto = _this5.Xauto;
			ws._watcher = _this5;
			ws._task = task;
			try {
				ws._handle = yield _this5._watcher.call(new Context(_this5.Xauto), ws.emit.bind(ws), _this5._opt, ..._this5._args);
				return new Proxy(ws, watchStateProxy);
			} catch (e) {
				handle.call(_this5.Xauto, e);
			}
		})();
	}
	/**
  * 基于当前监视者创建
  * @param  {any}		opt		选项
  * @param  {...any}		args	参数
  * @return {Watcher}			创建的监视者
  */
	new(opt, ...args) {
		return new Proxy(new Watcher(this, opt, ...args), watcherProxy);
	}
}

class Creator extends Function {
	constructor(creator, opt, ...args) {
		super();
		if (typeof opt !== "object") {
			opt = {};
		};
		if (creator instanceof Creator) {
			this._creator = creator._creator;
			this.Xauto = creator.Xauto;
			this._opt = Object.assign({}, creator._opt, opt);
			let _args = this._args = Object.create(creator._args);
			args.map(x => _args.push(x));
		} else if (creator instanceof Array && isFunction(creator[0]) && creator[1] instanceof Xauto) {
			this._creator = creator[0];
			this.Xauto = creator[1];
			this._opt = Object.assign({}, opt);
			let _args = this._args = [];
			args.map(x => _args.push(x));
		} else {
			throw new Error("非法的creator");
		}
	}
	/**
  * 创建任务
  * @param  {Object}		cfg		创建任务的参数
  * @return {Task}				创建的任务
  */
	create(cfg) {
		var _this6 = this;

		return _asyncToGenerator(function* () {
			let task = yield _this6._creator.call(Context(null, _this6.Xauto), cfg, _this6._opt, ..._this6._args);
			if (typeof task === "function") {
				task = new Proxy(new Task([task, _this6.Xauto]), taskProxy);
			} else if (!(task instanceof Task)) {
				task = null;
			}
			return task;
		})();
	}
	/**
  * 基于当前创建者创建
  * @param  {any}    opt		选项
  * @param  {...any} args	参数
  * @return {Creator}		创建的创建者
  */
	new(opt, ...args) {
		return new Proxy(new Creator(this, opt, ...args), creatorProxy);
	}
}
class Xauto {
	/**
  * 构造Xauto
  * @param	{Object}	options.opt		插件配置
  * @param	{Object}	options.pkg		程序包配置
  * @param	{Object}	options.path	路径信息
  * @param	{Function}	options.echo	信息输出函数
  * @param	{String}	options.XBWP	Xauto基本工作路径(即将废除)
  * @param	{String}	options.XPBP	Xauto插件基本路径(即将废除)
  */
	constructor({
		opt = {},
		pkg = {},
		path: paths,
		echo = print,
		XBWP, XPBP
	} = {}) {
		if (!opt || typeof opt !== "object") {
			opt = {};
		}
		if (!pkg || typeof pkg !== "object") {
			pkg = {};
		}
		if (!paths || typeof paths !== "object") {
			paths = {};
		}
		let { work: warkPath, plugin: pluginPath } = paths;
		if (typeof echo !== "function") {
			echo = print;
		}

		if (typeof XBWP !== "string") {
			XBWP = "";
		}
		if (typeof XPBP !== "string") {
			XPBP = "";
		}
		if (typeof warkPath !== "string") {
			warkPath = XBWP;
		}
		if (typeof pluginPath !== "string") {
			pluginPath = XPBP;
		}
		if (typeof warkPath !== "string") {
			warkPath = "";
		}
		if (typeof pluginPath !== "string") {
			pluginPath = "plugin";
		}
		if (!path.isAbsolute(warkPath)) {
			warkPath = path.resolve(process.cwd(), warkPath);
		}
		if (!path.isAbsolute(pluginPath)) {
			pluginPath = path.resolve(warkPath, pluginPath);
		}
		Object.defineProperties(this, {
			_task: { value: {}, enumerable: true },
			_creator: { value: {}, enumerable: true },
			_watcher: { value: {}, enumerable: true },
			_watchState: { value: {}, enumerable: true },
			_plugin: { value: new Set(), enumerable: true },

			_print: { value: echo, enumerable: true },

			opt: { value: Object.freeze(Object.assign({}, opt)), enumerable: true },
			pkg: { value: Object.freeze(Object.assign({}, pkg)), enumerable: true },
			path: { value: { work: warkPath, plugin: pluginPath }, enumerable: true },
			XBWP: { value: warkPath, enumerable: true },
			XPBP: { value: pluginPath, enumerable: true },

			util: { value: {}, enumerable: true },

			creator: { value: this.creator, enumerable: true },
			task: { value: this.task, enumerable: true },
			watcher: { value: this.watcher, enumerable: true },
			watchState: { value: this.watchState, enumerable: true },
			register: { value: this.register, enumerable: true },

			plugin: { value: this.plugin, enumerable: true },

			exec: { value: this.exec, enumerable: true },
			throw: { value: this.throw, enumerable: true },
			exit: { value: this.exit, enumerable: true },

			signal: { value: this.signal, enumerable: true },
			Signal: { value: this.signal, enumerable: true }
		});
	}
	/**
  * 创建或获取任务
  * @param	{any}		task	用于创建或获取任务的配置
  * @param	{String}	name	注册名
  * @param	{Boolean}	force	强制注册
  * @return	{Task}				任务
  */
	task(task, name, force) {
		var _this7 = this;

		return _asyncToGenerator(function* () {
			let tasks = _this7._task;
			if (typeof task === "string") {
				//获取任务
				if (task in tasks) {
					task = tasks[task];
				} else {
					task = null;
				}
			} else if (task instanceof Task) {
				task = task;
			} else if (task instanceof Array) {
				//使用已有任务创建任务
				let [t, opt, ...args] = task;
				task = t;
				if (typeof task === "string") {
					//从库中找出任务
					if (task in tasks) {
						task = tasks[task];
					} else {
						task = null;
					}
				} else if (task instanceof Array && (opt === undefined || opt === true)) {
					//使用基础创建者创建一个串联任务任务
					task = [yield baseCreator.call(_this7, { task: task, strict: opt === true }), _this7];
				} else if (isFunction(task)) {
					//使用普通函数创建任务
					task = [task, _this7];
				} else if (!(task instanceof Task)) {
					task = null;
				}
				if (task) {
					task = new Proxy(new Task(task, opt, ...args), taskProxy);
				}
			} else if (isFunction(task)) {
				//使用普通函数创建任务
				task = new Proxy(new Task([task, _this7]), taskProxy);
			} else if (typeof task === "object") {
				//使用创建者创建一个任务
				let creator = task[''];
				if (!creator) {
					creator = task.$;
				}
				if (!creator) {
					creator = task.creator;
				}
				creator = yield _this7.creator(creator);
				if (!creator) {
					task = null;
				}
				if (task) {
					task = yield creator(task);
				}
			} else {
				task = null;
			}
			//任务注册
			if (task && isName(name) && (force || !(name in tasks))) {
				tasks[name] = task;
			}
			return task;
		})();
	}
	/**
  * 创建或获取监视者
  * @param	{any}		watcher	用于创建或获取监视者的配置
  * @param	{String}	name	注册名
  * @param	{Boolean}	force	强制注册
  * @return	{Watcher}			监视者
  */
	watcher(watcher, name, force) {
		var _this8 = this;

		return _asyncToGenerator(function* () {
			const List = _this8._watcher;
			if (typeof watcher === "string") {
				if (watcher in List) {
					watcher = List[watcher];
				} else {
					watcher = null;
				}
			} else if (watcher instanceof Watcher) {
				watcher = watcher;
			} else if (isFunction(watcher)) {
				watcher = [watcher, _this8];
				watcher = new Proxy(new Watcher(watcher), watcherProxy);
			} else if (watcher instanceof Array) {
				let [w, opt, ...args] = watcher;
				watcher = w;
				if (typeof watcher === "string") {
					if (watcher in List) {
						watcher = List[watcher];
					} else {
						watcher = null;
					}
				} else if (isFunction(watcher)) {
					watcher = [watcher, _this8];
				} else if (!(watcher instanceof Task || isFunction(watcher))) {
					watcher = null;
				}
				if (watcher) {
					watcher = new Proxy(new Watcher(watcher, opt, ...args), watcherProxy);
				}
			} else {
				watcher = null;
			}
			if (watcher && isName(name) && (force || !(name in List))) {
				List[name] = watcher;
			}
			return watcher;
		})();
	}

	/**
  * 获取监视状态
  * @param	{String}	name	监视状态名
  * @return	{Watcher}			监视状态
  */
	watchState(watchState) {
		var _this9 = this;

		return _asyncToGenerator(function* () {
			const List = _this9._watchState;
			if (typeof watchState === "string") {
				if (watchState in List) {
					watchState = List[watchState];
				} else {
					watchState = null;
				}
			} else {
				watchState = null;
			}
			return watchState;
		})();
	}
	/**
  * 创建或获取创建者
  * @param	{any}		watcher	用于创建或获取创建者的配置
  * @param	{String}	name	注册名
  * @return	{Creator}			创建的创建者
  */
	creator(creator, name, force) {
		var _this10 = this;

		return _asyncToGenerator(function* () {
			const List = _this10._creator;
			if (typeof creator === "string") {
				if (creator in List) {
					creator = List[creator];
				} else {
					creator = null;
				}
			} else if (creator instanceof Creator) {
				creator = creator;
			} else if (isFunction(creator)) {
				creator = new Proxy(new Creator([creator, _this10]), creatorProxy);
			} else if (creator instanceof Array) {
				let [c, opt, ...args] = creator;
				creator = c;
				if (typeof creator === "string") {
					if (creator in List) {
						creator = List[creator];
					} else {
						creator = null;
					}
				} else if (isFunction(creator)) {
					creator = [creator, _this10];
				} else if (!(creator instanceof Task)) {
					creator = null;
				}
				if (creator) {
					creator = new Proxy(new Creator(creator, opt, ...args), creatorProxy);
				}
			} else {
				creator = null;
			}
			if (creator && isName(name) && (force || !(name in List))) {
				List[name] = creator;
			}
			return creator;
		})();
	}
	/**
  * 注册创建者/任务/监视者/监视状态
  * @param	{Object}	obj		要注册的任创建者/务/监视者/监视状态
  * @param	{String}	name	注册名
  * @return	{Boolean}			是否注册成功
  */
	register(obj, name, force) {
		var _this11 = this;

		return _asyncToGenerator(function* () {
			let list = null;
			if (obj instanceof Task) {
				list = _this11._task;
			} else if (obj instanceof Watcher) {
				list = _this11._watcher;
			} else if (obj instanceof WatchState) {
				list = _this11._watchState;
			} else if (obj instanceof Creator) {
				list = _this11._creator;
			} else {
				return false;
			}
			if (obj.Xauto !== _this11) {
				return false;
			}
			if (!isName(name)) {
				return false;
			}
			if (!force && name in list) {
				return false;
			}
			list[name] = obj;
			return true;
		})();
	}
	/**
  * 装载插件(插件只会装载一次)
  * @param	{String}	name	插件名
  * @return	{Boolean}			是否已经载入且载入成功
  */
	plugin(name) {
		var _this12 = this;

		return _asyncToGenerator(function* () {
			let List = _this12._plugin;
			if (typeof name !== "string") {
				return false;
			}
			if (!name) {
				return false;
			}
			if (name === "Xauto") {
				return true;
			}
			if (List.has(name)) {
				return true;
			}
			let plugin = yield loadPluginModule(name, _this12.path.plugin);
			if (!plugin) {
				return false;
			}
			try {
				if (typeof plugin === "function") {
					plugin = yield plugin(_this12);
				} else if (plugin && typeof plugin.default === "function") {
					plugin = yield plugin.default(_this12);
				} else if (plugin && typeof plugin.init === "function") {
					plugin = yield plugin.init(_this12);
				}
			} catch (e) {
				return false;
			}
			if (!(plugin instanceof Object)) {
				return false;
			}
			let { task, creator, watcher } = plugin;
			if (task) {
				for (let k in task) {
					yield _this12.task(task[k], `${name}:${k}`);
				}
			}
			if (creator) {
				for (let k in creator) {
					yield _this12.creator(creator[k], `${name}:${k}`);
				}
			}
			if (watcher) {
				for (let k in watcher) {
					yield _this12.watcher(watcher[k], `${name}:${k}`);
				}
			}
			({ task, creator, watcher } = plugin);
			if (task) {
				for (let k in task) {
					yield _this12.task(task[k], `${name}:${k}`);
				}
			}
			if (creator) {
				for (let k in creator) {
					yield _this12.creator(creator[k], `${name}:${k}`);
				}
			}
			if (watcher) {
				for (let k in watcher) {
					yield _this12.watcher(watcher[k], `${name}:${k}`);
				}
			}
			List.add(name);
			return true;
		})();
	}
	/**
  * 执行一个任务
  * @param	{Task}		task	要执行的任务
  * @return	{Object}			任务结束返回的值
  */
	exec(task, cfg, context) {
		var _this13 = this;

		return _asyncToGenerator(function* () {
			if (!(task = _this13.task(task))) {
				return false;
			}
			try {
				yield task(cfg, context);
			} catch (e) {
				handle.call(_this13, e);return false;
			}
			return true;
		})();
	}
	/**
  * 开始监视
  * @param	{Watcher}		watcher	监视者
  * @param	{Task}			task	任务
  * @return	{WatchState}			监视状态
  */
	watch(watcher, task) {
		var _this14 = this;

		return _asyncToGenerator(function* () {
			if (!(watcher = yield _this14.watcher(watcher))) {
				return null;
			}
			if (!(task = yield _this14.task(task))) {
				return null;
			}
			return yield watcher(task);
		})();
	}
	/**
  * 输出信息
  * @param  {String} options.lv 等级
  * @param  {String} options.tp 类型
  * @param  {String} options.at 所在模块
  * @param  {String} options.fn 文件名
  * @param  {Number} options.ln 行号
  * @param  {Number} options.cl 列号
  * @param  {String} options.tx 错误信息
  */
	echo({ lv = "info", tp, at, fn, ln, cl, tx } = {}) {
		if (typeof lv === "string") {
			lv = lv.replace(/[^a-zA-Z0-9 \-_]+/g, "").replace(/ /g, "-");
		} else {
			lv = "";
		}
		if (typeof tp === "string") {
			tp = tp.replace(/[^a-zA-Z0-9 \-_]+/g, "").replace(/ /g, "-");
		} else {
			tp = "";
		}
		if (typeof at === "string") {
			at = at.replace(/[^a-zA-Z0-9 \-_\:]+/g, "").replace(/ /g, "-");
		} else {
			at = "";
		}
		if (typeof fn === "string") {
			fn = fn.replace(/[\r\n\t\f\:"'`]+/g, "");
		} else {
			fn = "";
		}
		if (!Number.isInteger(Number(ln))) {
			ln = "";
		}
		if (!Number.isInteger(Number(cl))) {
			cl = "";
		}
		if (typeof tx !== "string") {
			tx = "";
		}
		this._print.call(this, { lv, tp, at, fn, ln, cl, tx });
	}
	/**
  * 抛出错误信息并结束当前任务
  */
	throw(output) {
		throw new XautoSignal.Error(output);
	}
	/**
  * 退出当前任务
  */
	exit() {
		throw new XautoSignal.Exit();
	}
}
Xauto.prototype.Signal = Xauto.prototype.signal = XautoSignal;

/**
 * 是否为函数
 * @param	{Object}	func	被判断的对象
 * @return	{Boolean}			是否为函数
 */
function isFunction(func) {
	if (func instanceof Xauto) {
		return false;
	} else if (func instanceof Context) {
		return false;
	} else if (func instanceof Task) {
		return false;
	} else if (func instanceof Creator) {
		return false;
	} else if (func instanceof Watcher) {
		return false;
	} else if (func instanceof WatchState) {
		return false;
	} else if (typeof func === "function") {
		return true;
	} else {
		return false;
	}
}
/**
 * 错误处理器
 * @param  {Object} e 意外的错误
 */
function handle(e) {
	if (e instanceof XautoSignal.Error) {
		this.echo(e);
	}
	if (e instanceof XautoSignal) {
		return;
	} else if (e instanceof Error) {
		this.echo({ lv: "error", tx: e.stack });
	} else if (e) {
		this.echo(e);
	}
}

module.exports = new Proxy(Xauto, XautoProxy);