#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
try{
args = parsArgs();
if (!args) {
	showHelp();
} else {
	if (args.help) {
		showHelp();
	}
	xauto(args).catch(console.log);
}
}catch(e) {
	console.log(e);
}

/**
 * 读取JSON文件
 * @param  {String}	fn	文件名
 * @return {Object}		解析后的JSON
 */
async function readJSON (fn) {
	let data = await new Promise(
		(resolve, reject) => fs.readFile(fn, "utf-8",
			(err, data) => err ? reject(err) : resolve(data)
		)
	);
	if(data.substr(0,2) == "#!" && data.indexOf("\n") != -1) {
		data = data.substr(data.indexOf("\n"));
	}
	return JSON.parse(data);
}
/**
 * 根据配置执行Xauto
 * @param  {Boolean} options.help   是否显示帮助文档
 * @param  {String}  options.file   配置文件
 * @param  {String}  options.cdir   配置文件夹
 * @param  {String}  options.dir
 * @param  {Array}   options.plugin 自动加载的插件
 * @param  {Array}   options.exec   自动执行的任务
 * @param  {Array}   options.watch  自动开始的监听
 * @param  {String}  options.script 脚本文件
 */
async function xauto ({
		help = false,
		file = "",
		cdir = "",
		dir = "",
		plugin = [],
		exec = [],
		watch = [],
		script = ""
	}) {
	let config; let rc; let pkg;
	//配置文件
	if (file) {try {config = await readJSON(file);} catch (e) {}}

	//配置/package文件搜索路径
	if (cdir) {
		while (!(rc && pkg)) {
			//rc文件
			if (!rc) {try {rc = await readJSON(path.resolve(cdir, ".xautorc"));} catch(e) {}}
			//包文件
			if (!pkg) {try {pkg = await readJSON(path.resolve(cdir, "package.json"));} catch(e) {}}
			//父路径
			let c = path.dirname(cdir);
			if (c === cdir) {break;}
			cdir = c;
		}
	}
	//初始化
	if (typeof config !== "object" || !config) {config = null;}
	if (typeof rc !== "object" || !rc) {rc = null;}
	if (typeof pkg !== "object" || !pkg) {pkg = null;}
	//配置文件作为默认配置
	if (!config && rc) {config = rc;}
	//包中配置为第二配置
	if (!config && pkg) {config = pkg.xauto;delete pkg.xauto;}
	if (typeof config !== "object" || !config) {config = {};}
	config.pkg = pkg;
	if(!config.path) {config.path = {};}
	//确定工作目录
	if(dir) {config.path.work = dir;}
	//切换至工作目录
	try{process.chdir(config.path.work);}catch(e) {}
	//创建Xauto
	const Xauto = await require("../")(config);
	//添加插件，执行任务，开始监视
	for(let i = 0, l = plugin.length; i < l; i++) {await Xauto.plugin(plugin[i]);}
	for(let i = 0, l = exec.length; i < l; i++) {await Xauto.exec(exec[i]);}
	for(let i = 0, l = watch.length; i < l; i++) {await Xauto.watch(watch[i][0], watch[i][1]);}

	console.log("Xauto基本工作路径: ", Xauto.path.work);
	//执行脚本
	if (script) {
		script = path.resolve(Xauto.path.work, script);
		try {script = require(script);} catch(e) {}
		if(typeof script === "function") {
			try {script(Xauto);} catch(e) {}
		}
	}
}

/**
 * 解析参数
 */
function parsArgs () {
	let help = false;
	let file = "";
	let dir = "";
	let setdir = false;
	let cdir = "";
	let exec = [];
	let plugin = [];
	let list = exec;
	let watch = [];
	let script;
	for (let i = 2, argv = process.argv, l = argv.length; i < l; i++) {
		let arg = argv[i];
		switch(arg) {
		case "-h": case "--help":
			//显示帮助
			help = true;
			break;
		case "-f": case "--config": case "--file":
			//配置文件
			arg = argv[++i];
			if(arg) {file = arg;}
			break;
		case "-d": case "--dir":
			//工作路径
			setdir = true;
			arg = argv[++i];
			if(arg) {dir = arg;}
			break;
		case "-c": case "--cfgdir":
			//配置搜索路径
			arg = argv[++i];
			if(arg) {cdir = arg;}
			break;
		case '-e': case "--exec":
			//自动执行的任务
			list = exec;
			break;
		case '-p': case "--plugin":
			//加在的插件
			list = plugin;
			break;
		default:
			if (list == exec && arg.search(/[\/\\]/) != -1) {
				file = arg;
				break;
			}
			list.push(arg);
			break;
		case "-w": case "--watch":
			//自动开始的监听
			let w = argv[++i];
			let t = argv[++i];
			watch.push([w, t]);
			break;
		case '-s': case "--script":
			//自动执行的脚本
			script = argv[++i];
			break;
		}
	}
	if (help && process.argv.length === 3) {return false;}
	if (file) {file = path.resolve("",file);}
	dir = path.resolve("", dir);
	cdir = path.resolve(dir, cdir);
	if(!setdir) {dir = null;}
	return {help, file, cdir, dir, plugin, exec, watch, script};
}

function showHelp () {
	let help = `Usage:
  xauto [task...] [-h] [-f config] [-d dir] [-c cfgdir]
        [-p plugin...] [-e task...] [-w watcher task] [-s script]

Options:
  task...                立即执行的任务名
  -h                     显示帮助文档
  -f config              配置文件，优先级高于 .xautorc
  -d dir                 设置工作文件夹
  -c cfgdir              开始寻找 .xautorc 或 package.json的路径,
                         如果是相对路径，则基于工作文件夹,
                         如果未找到, 则在父文件夹中寻找,
                         直到找到其中一者或到达根目录
  -e task...             立即执行的任务名
  -p plugin...           自动载入的插件
  -w watcher task        自动开始的监听, watcher表示监视者名称, task表示任务名称
  -s script              立即执行的脚本路径
`
	console.log(help);
}
