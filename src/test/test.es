const Xauto = require("../");
let xauto;
async function copy(from, to) {
	xauto = await Xauto();
	await xauto.plugin("file");
	let task = await xauto.task([[
		["file:read", {dir: from}],
		["file:write", {dir: to}]
	]],'file');
	let watcher = await xauto.watcher(["file:watch", {dir:"from"}], "file");
	await watcher(task);
}

copy('from', 'to').catch(console.log);