在CLI中使用Xauto
========

##全局安装Xauto
使用npm全局安装

```
npm install xauto -g
```

##安装Xauto插件
比如要安装`file`和`less`插件，则将插件安装到项目目录下

```
npm install xauto-plugin-file
npm install xauto-plugin-less
```
如果Xauto全局安装，则插件也可全局安装

```
npm install xauto-plugin-file -g
npm install xauto-plugin-less -g
```

##配置Xauto
Xauto配置是JSON格式  
可以直接保存到项目根目录下的`.xautorc`文件中，也可保存到`package.json`文件的`xauto`。
以将`less`文件夹下less转为`css`文件夹下css为例, 在`.xautorc`中配置的示例：

```
{
    "plugin": ["file", "less"], //预载入的插件，安装方式将上一节
    "task": {   //预设值任务，格式为："任务名称": 任务描述(详见Xauto 描述)
        "file": { 
            "": "Xauto:base",   //这里使用了注册名为"Xauto:base"的创建者
                                //下面的为创建者需要的参数，具体见创建者的说明
            "task": [   //"Xauto:base"创建者需要一个task数组，各成员均为任务描述
                ["file:read", {"dir": "less"}], 
                ["less:render"],
                ["file:write", {"dir": "css"}] 
            ] 
        } 
    }, 
    "watcher": {    //预设值监视者，格式为："监视者名称": 监视者描述(详见Xauto 描述)
        "created":["file:created", {"dir":"less"}],
        "changed":["file:changed", {"dir":"less"}] 
    }, 
    "watch": {      //预启动监视，格式为："监视状态名称": ["监视者名称", "任务名称"]
        "createdFile":["created", "file"], 
        "changedfile":["changed", "file"] 
    } 
}
```

##启动Xauto
```
xauto
```

##xauto命令参数
```
xauto [task...] [-h] [-f config] [-d dir] [-c cfgdir] [-e task...] [-p plugin...] [-w watcher task] [-s script]
```
参数说明如下: 
* `task...` - 立即执行的任务名
* `-h` - 显示帮助文档
* `-f config` - 配置文件，优先级高于 .xautorc
* `-d dir` - 设置工作文件夹
* `-c cfgdir` - 开始寻找 `.xautorc` 或 `package.json`的路径,如果是相对路径，则基于工作文件夹, 如果未找到, 则在父文件夹中寻找, 直到找到其中一者或到达根目录
* `-e task...` - 立即执行的任务名
* `-p plugin...` - 自动载入的插件
* `-w watcher task` - 自动开始的监听, watcher表示监视者名称, task表示任务名称
* `-s script` - 立即执行的脚本路径

