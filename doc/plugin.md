Xauto 插件开发
========
##什么是Xauto插件
Xauto插件即一个Node包，命名规则为`xauto-plugin-插件名`, 如`file`插件, 包名为`xauto-plugin-file`。  
Xauto插件应导出一个函数或`Object`对象。

##导出为object
导出为object可以有以下成员: 
* `creator`{Object<创建者描述>} - *可选的*导出的创建者
* `task`{Object<任务描述>} - *可选的*导出的任务
* `watcher`{Object<监视者描述>} - *可选的*导出的监视者

##导出为函数
如果导出为一个函数，则函数的第一个参数为所在的`Xauto`对象, 返回值为应为`Object`对象, 处理方式同直接导出的`Object`对象。


