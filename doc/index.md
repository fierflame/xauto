![Xauto](image/xauto.png)  
Xauto 是易扩展的自动化工具。  
Xauto 是一个任何事情都可以做的工具, 同时也是一个什么事情也做不了的工具。  
Xauto 自身不能做任何事情，只负责一个任务的管理。  
但只要有适当的插件或脚本，就可以做你想做的任何事情。  
[![npm|Xauto](https://img.shields.io/npm/v/xauto.svg)](https://www.npmjs.com/package/xauto)
[![npm|Xauto](https://img.shields.io/npm/l/xauto.svg)](https://www.npmjs.com/package/xauto)
[![npm|Xauto](https://img.shields.io/npm/dt/xauto.svg)](https://www.npmjs.com/package/xauto)

##安装Xauto
使用npm 安装

```
npm install xauto -g
```

##系统依赖
node 6.9.0 或更高版本。

##帮助文档
* [在CLI中使用 Xauto](cli.md)
* [Xauto 描述](description.md)
* [Xauto API](api.md)
* [Xauto 对象简写调用对应的方法](logogram.md)
* [Xauto 插件开发](plugin.md)

## 注意事项
* 不要使用未提到的方式（包括但不限于描述、API）使用，即便现在能够按照你的预期效果执行，但在将来的升级中，可能不再支持。

