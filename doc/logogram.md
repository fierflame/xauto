Xauto对象简写调用对应的方法
========
|Class     |f(...)      |new f(...)|
|----------|------------|----------|
|Task      |.exec(...)  |.new(...) |
|Creator   |.create(...)|.new(...) |
|Watcher   |.watch(...) |.new(...) |
|WatchState|.stop(...)  |          |