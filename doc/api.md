Xauto API
========
##Xauto
###async Xauto([options])
异步创建Xauto对象，并预执行一下操作:
1. 创建Xauto创建者
2. 加载预加载插件
3. 创建预配置创建者
4. 创建预配置任务
5. 创建预配置监视者
6. 执行自动执行的任务(不等待任务结束)
7. 开始监听自动开始的监听
8. 执行自动执行脚本
9. 如果脚本导出函数, 创建的Xauto对象作为参数执行函数

有效的选项[options]如下:
* `opt`{Object} - *可选的* 插件配置，插件默认配置在此配置内，具体见各插件说明
* `pkg`{Object} - *可选的* 程序包配置，包的package.json信息
* `path`{Object} - 路径信息，包括工作路径`path.work`和插件路径`path.plugin`
* `echo`{Function} - *可选的* 信息输出函数
* `XBWP`{String} - *可选的* *即将废弃* Xauto基本工作路径，请用`path.work`代替
* `XPBP`{String} - *可选的* *即将废弃* Xauto插件基本搜索路径，请用`path.plugin`代替
* `plugin`{Array<String>} - *可选的* 预加载的插件名
* `task`{Object<任务描述>} - *可选的* 预配置的任务
* `watcher`{Object<监视者描述>} - *可选的* 预配置的监视者
* `exec`{Array<String>} - *可选的* 自动执行的任务名
* `watch`{Object<[watcher, task]>} - *可选的* 自动开始的监听
* `script`{String} - *可选的* 自动执行的脚本路径

###new Xauto([options])
`不建议`直接使用Xauto的构造函数创建Xauto对象，请用Xauto()异步创建Xauto对象  
有效的选项[options]如下:
* `opt`{Object} - *可选的* 插件配置，插件默认配置在此配置内，具体见各插件说明
* `pkg`{Object} - *可选的* 程序包配置，包的package.json信息
* `path`{Object} - 路径信息，包括工作路径`path.work`和插件路径`path.plugin`
* `echo`{Function} - *可选的* 信息输出函数
* `XBWP`{String} - *可选的* *即将废弃* Xauto基本工作路径，请用`path.work`代替
* `XPBP`{String} - *可选的* *即将废弃* Xauto插件基本搜索路径，请用`path.plugin`代替

###xauto.opt
`只读` 此属性为只读属性  
创建Xauto时传入的opt  

###xauto.pkg
`只读` 此属性为只读属性  
创建Xauto时传入的pkg  

###xauto.path
`只读` 此属性为只读属性  
创建Xauto时传入的path  
其中:
* `work`{String} - *只读* 工作路径  
  如果是相对路径，将会根据创建对象时的工作路径转为绝对路径  
* `plugin`{String} - *只读* 插件路径  
  如果是相对路径，将会根据XPBP转为工作路径  

###xauto.XBWP
`只读` 此属性为只读属性  
`即将废弃` 此属性即将废弃  
创建Xauto时传入的XBWP  
如果是相对路径，将会根据创建对象时的工作路径转为绝对路径  

###xauto.XPBP
`只读`此属性为只读属性  
`即将废弃` 此属性即将废弃  
创建Xauto时传入的XPBP  
如果是相对路径，将会根据XPBP转为绝对路径  

###xauto.util
由插件导入的工具集

###async xauto.task(task, [name])
**异步**创建或获取`Task(任务)`
* `task`{任务描述} - 用于创建或获取任务的配置
* `name`{String} - *可选的*注册名

注意：
* 如果为字符串，则为获取。
* 如果创建或获取失败，则返回`null`。

###async xauto.watcher(watcher, [name])
**异步**创建或获取`Watcher(监视者)`
* `watcher`{监视者描述} - 用于创建或获取监视者的配置
* `name`{String} - *可选的*注册名

注意：
* 如果为字符串，则为获取。
* 如果创建或获取失败，则返回`null`。

###async xauto.watchState(watchState)
**异步**获取`WatchState(监视状态)`
* `name`{String} - 监视状态名

注意：
* 如果取失败，则返回`null`。

###async xauto.creator(creator, [name])
**异步**创建或获取`Creator(创建者)`
* `creator`{创建者描述} - 用于创建或获取创建者的配置
* `name`{String} - *可选的*注册名

注意：
* 如果为字符串，则为获取。  
* 如果创建或获取失败，则返回`null`。  

###async xauto.register(obj, name)
**异步**注册`Task(任务)`/`Watcher(监视者)`/`/WatchState(监视状态)`/`Creator(创建者)`
* `obj`{Task/Watcher/WatchState/Creator} - 要注册的任务/监视者/监视状态/创建者
* `name`{String} - 注册名

返回值表示是否注册成功  

###async xauto.plugin(name)
**异步**装载插件(插件只会装载一次)
* `name`{String} - 插件名

返回值表示是否已经载入且载入成功  

###async xauto.exec(task, [cfg, [context]])
**异步**执行任务
* `task`{Task/String} - 要执行的任务或任务名
* `cfg`{any} - *可选的*传入任务的配置
* `context`{Context} - *可选的*执行任务的上下文

返回值为任务结束返回的值  
`xauto.exec()`是在`task.exec()`的外层加了try...catch, 
如果`task.exec()`抛出错误将会被处理，然后返回`undefind`.

###async xauto.watch(watcher, task)
**异步**开启监视
* `watcher`{Watcher/String} - 所使用的监视者
* `task`{Task/String} - 监视激活执行的任务或任务名

返回值为监视状态

###xauto.Signal(signal), xauto.Signal.TYPE(signal)
*同步*过滤信号
* `signal` - 当做信号过滤的错误。如果是指定类型的信号，将会被重新抛出。

注意:
* `xauto.Signal(signal)` 和 `xauto.Signal.TYPE(signal)` 应该在catch块的开始使用。

###xauto.echo([options]) 
*同步*输出信息  
有效的选项[options]如下:
* `lv`{String} - *可选的*等级
* `tp`{String} - *可选的*类型
* `at`{String} - *可选的*所在插件
* `fn`{String} - *可选的*文件名
* `ln`{Number} - *可选的*行号
* `cl`{Number} - *可选的*列号
* `tx`{String} - *可选的*错误信息

###xauto.throw([options]) 
*同步*输出信息并终止任务
选项[options]同`xauto.echo([options])` 选项

###xauto.exit() 
*同步*终止任务

`xauto.echo()`, `xauto.throw()`与`xauto.exit()`的共同点与区别及注意事项：  
* `xauto.echo()` 与 `xauto.throw()`都会输出信息, 但`xauto.throw()`会抛出一个信号`XautoSignal.Error`， 此信号可以被插件或用户脚本通过try捕获并处理， 如果信号被捕获，将不会输出信息。
* `xauto.throw()`与`xauto.exit()`都是通过抛出`XautoSignal`的子类信号来终止任务此信号可以被插件或用户脚本通过catch捕获并处理， 如果信号被捕获，将不会终止任务。
* 如果不想捕获`XautoSignal`信号，可以在catch块的开始通过`xauto.Signal`处理信号，如果只是不想处理某类信号，可以使用`xauto.Signal.TYPE`处理，如`xauto.Signal.Exit`



##Context(相关上下文)
各函数执行时的`this`, 原型为`xauto`  


##Task(任务)
使用`xauto.task()`创建或获取  

###async task.exec([cfg, [context]])
**异步**执行此任务 
* `cfg`{any} - *可选的*任务的配置 
* `context`{Context} - *可选的*上下文this 

返回值为任务的返回值  

###task.new([opt, [...args]])
*同步*基于当前任务创建 
* `opt`{Object} - *可选的*选项, 在原有选项基础上, 加上新的选项, 如果选项重复, 则会用新选项覆盖旧选项
* `args`{...any} - *可选的*参数, 添加在原有参数之后

返回值为创建的任务 

###async task([cfg, [context]])
`async task.exec([cfg, [context]])`的简写

###new task([opt, [...args]])
`async task.new([opt, [...args]])`的简写


##Creator(创建者)
使用`xauto.creator()`创建或获取

###async creator.create(cfg)
`不推荐`**异步**创建任务 
* `cfg`{Object} - 任务的参数 

返回值为创建的任务  

###creator.new([opt, [...args]])
*同步*基于当前任务创建者  
* `opt`{Object} - *可选的*选项, 在原有选项基础上, 加上新的选项, 如果选项重复, 则会用新选项覆盖旧选项
* `args`{...any} - *可选的*参数, 添加在原有参数之后

返回值为创建的创建者 

###async creator(cfg)
`async creator.exec(cfg)`的简写

###new creator([opt, [...args]])
`async creator.new([opt, [...args]])`的简写


##Watcher(监视者)
使用`xauto.watcher()`创建或获取

###async watcher.watch(task)
**异步**开始监视 
* `task`{Task} - 监听事件触发的任务

返回值为创建的任务  

###watcher.new([opt, [...args]])
*同步*基于当前任务监视者  
* `opt`{Object} - *可选的*选项, 在原有选项基础上, 加上新的选项, 如果选项重复, 则会用新选项覆盖旧选项
* `args`{...any} - *可选的*参数, 添加在原有参数之后

返回值为创建的监视者 

###async watcher(task)
`async watcher.watch(task)`的简写

###new watcher([opt, [...args]])
`async watcher.new([opt, [...args]])`的简写


##WatchState(监视状态)
使用`xauto.watchState()`获取或使用`xauto.watch(watcher, task)`创建

###async watchState.stop()
**异步**停止监视 

###async watchState()
`async watchState.stop()`的简写

